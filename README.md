# 自用图标导航
## 说明 
<br>

   **⚠️图标为Quantumult_X 144*144格式**

<br>

## App图标
<br>

| 123云盘 | 高德地图 | Apple | AppleTV |
| --- | --- | --- | --- |
| <img src="https://gitlab.com/xiaozeng/Icon/-/raw/main/App/123pan.png" alt="123云盘" width="80"/> | <img src="https://gitlab.com/xiaozeng/Icon/-/raw/main/App/Amap.png" alt="高德地图" width="80"/> | <img src="https://gitlab.com/xiaozeng/Icon/-/raw/main/App/Apple.png" alt="Apple" width="80"/> | <img src="https://gitlab.com/xiaozeng/Icon/-/raw/main/App/AppleTV.png" alt="XLM" width="80"/> |
| [长按复制](https://gitlab.com/xiaozeng/Icon/-/raw/main/App/123pan.png) | [长按复制](https://gitlab.com/xiaozeng/Icon/-/raw/main/App/Amap.png) | [长按复制](https://gitlab.com/xiaozeng/Icon/-/raw/main/App/Apple.png) | [长按复制](https://gitlab.com/xiaozeng/Icon/-/raw/main/App/AppleTV.png) |
<br>

## 机场图标

| ACA | M78star | 花云 | XLM |
| --- | --- | --- | --- |
| <img src="https://gitlab.com/xiaozeng/Icon/-/raw/main/Airport/ACA.png" alt="ACA" width="80"/> | <img src="https://gitlab.com/xiaozeng/Icon/-/raw/main/Airport/m78star.png" alt="M78star" width="80"/> | <img src="https://gitlab.com/xiaozeng/Icon/-/raw/main/Airport/Flowercloud.png" alt="花云" width="80"/> | <img src="https://gitlab.com/xiaozeng/Icon/-/raw/main/Airport/XLM.png" alt="XLM" width="80"/> |
| [长按复制](https://gitlab.com/xiaozeng/Icon/-/raw/main/Airport/ACA.png) | [长按复制](https://gitlab.com/xiaozeng/Icon/-/raw/main/Airport/m78star.png) | [长按复制](https://gitlab.com/xiaozeng/Icon/-/raw/main/Airport/Flowercloud.png) | [长按复制](https://gitlab.com/xiaozeng/Icon/-/raw/main/Airport/XLM.png) |

## Country图标

| 台湾 |  |
| --- | --- |
| <img src="https://gitlab.com/xiaozeng/Icon/-/raw/main/Country/TW.png" alt="台湾" width="80"/> |  |
| [长按复制](https://gitlab.com/xiaozeng/Icon/-/raw/main/Country/TW.png) |  |
